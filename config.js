let config = {};
config.web = {};

config.web.url = process.env.URL || "http://kanboard/";
config.web.headless = process.env.HEADLESS || false;
config.web.networkSubscription = process.env.NETWORK;

module.exports = config;
