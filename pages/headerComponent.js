import {expect} from "@playwright/test";
import {UserMenuComponent} from "./userMenuComponent";
import {AddProjectModal} from "./addProjectModal";

export class HeaderComponent {
    constructor(page) {
        this.page = page;
        this.header = "xpath=.//header"
        this.logo = "xpath=.//header//*[contains(@class, 'logo')]"
        this.dropdownLink = "xpath=.//header//*[contains(@class, 'avatar-inline')]"
        this.addNewProject = "xpath=.//section//a[text()='New project']"
        this.waitForLoading();
    }

    waitForLoading() {
        const logo = this.page.locator(this.logo);
        expect(logo).toBeVisible();
    }

    async openUserMenu() {
        await this.page.locator(this.dropdownLink).click();
        return new UserMenuComponent(this.page);
    }

    async openAddProjectModal() {
        await this.page.locator(this.addNewProject).click();
        return new AddProjectModal(this.page);
    }

    async checkProjectName(name) {
        const projectNameXpath = `${this.header}//*[contains(text(), '${name}')]`
        const projectName = await this.page.locator(projectNameXpath);
        return await expect(projectName).toBeVisible();
    }


}