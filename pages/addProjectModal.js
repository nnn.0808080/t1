import {ProjectPage} from "./projectPage";
import {expect} from "@playwright/test";

export class AddProjectModal {
    constructor(page) {
        this.page = page;
        this.addNewProjectForm = "#project-creation-form"
        this.nameInput = "#form-name"
        this.buttonSave = "xpath=.//button[text()='Save']"
        this.waitForLoading();
    }

    waitForLoading() {
        const modal = this.page.locator(this.addNewProjectForm);
        expect(modal).toBeVisible();
    }

    async addNewProject(name) {
        await this.page.locator(this.nameInput).fill(name);
        await this.page.locator(this.buttonSave).click();
        return new ProjectPage(this.page);
    }
}
//module.exports = { DashboardPage };