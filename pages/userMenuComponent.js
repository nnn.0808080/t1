import {expect} from "@playwright/test";

export class UserMenuComponent {
    constructor(page) {
        this.page = page;
        this.userMenu = "xpath=.//*[contains(@class, 'dropdown-submenu-open')]"
        this.waitForLoading();
    }

    waitForLoading() {
        const userMenu = this.page.locator(this.userMenu);
        expect(userMenu).toBeVisible();
    }

    async checkLogin(login) {
        const userMenu = await this.page.locator(this.userMenu);
        return await expect(userMenu).toContainText(login);
    }
}