import {expect} from "@playwright/test";
const { DashboardPage } = require("./dashboardPage");

export class AuthorizationPage {
    constructor(page) {
        this.page = page;
        this.loginInput = "#form-username"
        this.passwordInput = "#form-password"
        this.buttonLogIn = "xpath=.//button"
        this.errorMessage = "xpath=.//*[text()='Bad username or password']"
        this.waitForLoading();
    }

    waitForLoading() {
        const loginInput = this.page.locator(this.loginInput);
        expect(loginInput).toBeVisible();
    }

    async authorize(username, password, result='success') {
        await this.page.locator(this.loginInput).fill(username);
        //await this.page.fill(this.loginInput, username);
        await this.page.locator(this.passwordInput).fill(password);
        await this.page.locator(this.buttonLogIn).click();
        if (result === 'success')
        {
            return new DashboardPage(this.page);
        }
        return null;
    }

    async checkErrorMessage() {
        const errorMessage = await this.page.locator(this.errorMessage);
        return await expect(errorMessage).toBeVisible();
    }
}
//module.exports = { AuthorizationPage };