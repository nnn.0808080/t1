import {expect} from "@playwright/test";

export class DashboardPage {
    constructor(page) {
        this.page = page;
        this.dashboard = "#dashboard"
        this.waitForLoading();
    }

    waitForLoading() {
        const dashboard = this.page.locator(this.dashboard);
        expect(dashboard).toBeVisible();
    }
}
//module.exports = { DashboardPage };