import {expect} from "@playwright/test";

export class ProjectPage {
    constructor(page) {
        this.page = page;
        this.projectHeader = "xpath=.//*[contains(@class, 'project-header')]"
        this.waitForLoading();
    }

    waitForLoading() {
        const projectHeader = this.page.locator(this.projectHeader);
        expect(projectHeader).toBeVisible();
    }
}
//module.exports = { DashboardPage };