const config = require("../config");
//const { chromium, base } = require("@playwright/test");
const {chromium} = require("@playwright/test");
const base = require('@playwright/test');

const test2 = base.test.extend({
    page: async ({ page }, use) => {
        let browser = await chromium.launch({
            headless: config.web.headless,
        });
        page = await browser.newPage();
        await page.goto(config.web.url);
        await page.waitForLoadState("domcontentloaded");

        await use(page);

        await page.close();
        await browser.close();
    }
});
exports.test2 = test2