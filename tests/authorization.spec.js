import {HeaderComponent} from "../pages/headerComponent";
const { test} = require('@playwright/test');
const { AuthorizationPage } = require("../pages/AuthorizationPage");
import { test2 } from "./fixture";


test.describe('authorization', () => {
    test2("success login", async ({ page }) => {
        const authorizationPage = new AuthorizationPage(page);
        await authorizationPage.authorize('admin', 'admin');
        const headerComponent = new HeaderComponent(page);
        const userMenu = await headerComponent.openUserMenu();
        await userMenu.checkLogin('admin');

    });

    test2("fail login", async ({ page }) => {
        const authorizationPage = new AuthorizationPage(page);
        await authorizationPage.authorize('admin', 'admi2', 'fail');
        await authorizationPage.checkErrorMessage();
    });
});