import {HeaderComponent} from "../pages/headerComponent";
const { test} = require('@playwright/test');
const { AuthorizationPage } = require("../pages/AuthorizationPage");
import { test2 } from "./fixture";


test.describe('addProject', () => {
    test2("add new project", async ({ page }) => {
        const authorizationPage = new AuthorizationPage(page);
        await authorizationPage.authorize('admin', 'admin');
        const headerComponent = new HeaderComponent(page);
        const addProjectModal = await headerComponent.openAddProjectModal();
        await addProjectModal.addNewProject('test');
        await headerComponent.checkProjectName('test')
    });
});
